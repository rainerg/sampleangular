'use strict';

angular.module('addressBook').factory('storage', Storage);

function Storage($localStorage, $http, $q) {

  function genId() {
    return 'xxxxxxxxxxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  function sync(data) {
    store.list = $localStorage.addressList = data;
  }

  var store = {
    list: [],

    getId: function (id) {
      return $localStorage.addressList.filter(function (val) {
        return val._id == id;
      })[0]
    },
    create: function () {
      return {_id: genId()};
    },
    save: function (id, item) {
      var storedItem = $localStorage.addressList.filter(function (val) {
        return val._id == id;
      })[0];

      if (storedItem) {
        $localStorage.addressList[$localStorage.addressList.indexOf(storedItem)] = item
      } else {
        $localStorage.addressList.push(item)
      }

    },
    remove: function (item) {
      $localStorage.addressList.splice($localStorage.addressList.indexOf(item), 1);
    },
    clearAll: function () {
      sync([])
    },
    importSample: function () {
      var deferred = $q.defer();
      $http.get("sampleData.json")
        .success(function (response) {
          sync(response);
          deferred.resolve()
        });

      return deferred.promise;
    }
  };

  sync($localStorage.addressList || []);

  return store;

}
