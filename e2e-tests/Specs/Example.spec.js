var EditPage = require('./../PageObjects/EditPage.js');
var HeaderPage = require('./../PageObjects/HeaderPage.js');
var ListPage = require('./../PageObjects/ListPage.js');


describe('Basic test example', function () {

  //Arrange PageObjects
  var editPage = new EditPage();
  var headerPage = new HeaderPage();
  var listPage = new ListPage();

  var params = browser.params;

  it('User should be able to see message "Your Address Book is Empty!!" when he will open Address book first time', function () {
    listPage.Get();
    var noDataMessage = element(by.css('div.header')).getText();

    expect(noDataMessage).toEqual('Your Address Book is Empty!!');
  });

  it('User should be able to see addresses after importing sample data', function () {
    listPage.Get();

    listPage.ImportSampleData();

    var count = element.all(by.repeater('address in addressList | search: searchText.$'))//.count();

    expect(count).not().toEqual(0);
  });

  it('User should be able to clear all imported samples in address book', function () {
    listPage.ClearAllData();
    var noDataMessage = element(by.css('div.header')).getText();

    expect(noDataMessage).toEqual('Your Address Book is Empty!!');

    var res = element.all(by.repeater('address in addressList | search:searchText.$')).count();

    expect(res).toEqual(0);
  });


  it('User should be able to create new address', function () {
    listPage.Get();
    listPage.GotoNewAddress();
    editPage.CreateNewAddress(
      params.happyPass.firstName,
      params.happyPass.lastName,
      params.happyPass.email,
      params.happyPass.address,
      params.happyPass.phone);


    listPage.Get();
    var res = element.all(by.repeater('address in addressList | search:searchText.$')).count();

    expect(res).toEqual(1);

  });


});






