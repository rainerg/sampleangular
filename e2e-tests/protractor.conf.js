exports.config = {

  allScriptsTimeout: 99999 ,

  seleniumAddress: 'http://localhost:4444/wd/hub',

  specs: ['./Specs/*.spec.js'],

  multiCapabilities: [
    {
      'browserName': 'chrome'
    }
  ],

  baseUrl: 'http://localhost:8000/',

  onPrepare: function(){
    browser.driver.manage().window().maximize();
  },

  framework: 'jasmine',

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    isVerbose : true,
    includeStackTrace : true
  },


  params: {
    happyPass:{
      firstName: 'testFirstName',
      lastName: 'tetsLastName',
      email: 'chubaka@starwars.com',
      address: 'planet Tatuin "Lord Sith" st. 15',
      phone: '555-55-55'
    }
  }




};
