function EditPage(){

	this.txtFirstName = element(by.model('address.name.first'));
	this.txtLastName = element(by.model('address.name.last'));
	this.txtEmail = element(by.model('address.email'));
	this.txtAddress = element(by.model('address.address'));
	this.txtxPhone = element(by.model('address.phone'));

	this.bntSave = element(by.css('._btnSaveAddress'));
	this.btnDelete = element(by.css('._btnDeleteAddress'));


	this.Get = function(){
		browser.get('/#/new/');
	};

	this.CreateNewAddress = function(firstName,lastName,email,address,phone){
		this.txtFirstName.sendKeys(firstName);
		this.txtLastName.sendKeys(lastName);
		this.txtEmail.sendKeys(email);
		this.txtAddress.sendKeys(address);
		this.txtxPhone.sendKeys(phone);

		this.bntSave.click();
	};


};

module.exports = EditPage;