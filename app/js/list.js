'use strict';

angular.module('addressBook.list', ['ngRoute'])

  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'views/list.html',
      controller: 'ListCtrl'
    });
  }])

  .controller('ListCtrl', ListCtrl)


function ListCtrl($scope, storage) {

  $scope.addressList = storage.list;

  $scope.importSample = function () {

    storage.importSample()
      .then(function () {
        $scope.addressList = storage.list;
      })

  };

  $scope.clearAll = function () {
    storage.clearAll();
    $scope.addressList = storage.list;
  };

  $scope.callphone = function () {
    alert('Seems you have no phone ))')
  };

};