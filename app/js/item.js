'use strict';

angular.module('addressBook.addressItem', ['ngRoute'])
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/edit/:id', {
        templateUrl: 'views/item.html',
        controller: 'EditCtrl'
      }).when('/new', {
        templateUrl: 'views/item.html',
        controller: 'NewCtrl'
      });
  }])

  .controller('EditCtrl', EditCtrl)
  .controller('NewCtrl', NewCtrl);

function EditCtrl($scope, $routeParams, $location, storage) {
  $scope.title = "Edit address";

  var originAddress = storage.getId($routeParams.id);

  $scope.address = angular.copy(originAddress);

  $scope.update = function () {
    storage.save($scope.address._id, $scope.address);
    $location.path("/");
  };

  $scope.delete = function () {
    storage.remove(originAddress);
    $location.path("/");
  }

}

function NewCtrl($scope, $location, storage) {
  $scope.title = "Create address";
  $scope.address = storage.create();


  $scope.update = function () {
    storage.save($scope.address._id, $scope.address);
    $location.path("/");
  };

  $scope.delete = function () {
    $location.path("/");
  }

}