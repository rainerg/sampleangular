'use strict';

angular.module('addressBook', [
  'ngRoute',
  'ngStorage',
  'addressBook.list',
  'addressBook.addressItem'
]).config(['$routeProvider', function ($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/'});
}]);
