'use strict';

describe('List ::', function() {

  beforeEach(module('addressBook.list'));

    it('should have ListCtrl', inject(function($controller) {
      //spec body
      var list = $controller('ListCtrl');
      expect(list).toBeDefined();
    }));

});