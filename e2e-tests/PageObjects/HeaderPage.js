function HeaderPage(){
	
	this.btnGotoHomePage = element(by.id('_btnhomePage'));
	this.txtSearch = element(by.model('searchText.$'));

	this.SearchContact = function(searchText){
		txtSearch.clear();
		txtSearch.sendKeys(searchText);
	};

	this.btnGotoHomePage = function(){
		btnGotoHomePage.click();
	};
};

module.exports = HeaderPage;