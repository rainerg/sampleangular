# Sample Address book
based on angular_seed
use [node] and [bower]



### Install Dependencies

run
```
npm install
```

### Run the Application

To start server
```
npm start
```

Now browse to the app at `http://localhost:8000/`.



### Testing app

Run protractor tests

```
protractor e2e-tests/protractor.conf.js
```

