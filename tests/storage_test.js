'use strict';

describe('Storage factory :: ', function () {

  beforeEach(module('addressBook'));


  it('should clear all records', inject(function (storage) {
    storage.clearAll()
    expect(storage.list.length).toEqual(0);
  }));

  it('should load sample data', inject(function (storage) {
    storage.importSample()
    expect(storage.list.length).not().toEqual(0);
  }));


});